# base class for all levels
extends Node2D

signal dropped
var win = preload("res://scenes/win.tscn").instance()

func _ready():
	self.connect("dropped", self, "on_dropped")
	self.set_process(true)

func _process(delta):
	var still_dragging = false
	for child in self.get_children():
		if(child.is_in_group("dragging")):
			still_dragging = true
	if(!still_dragging):
		show_win_and_clear_level()

func show_win_and_clear_level():
	set_process(false)
	self.add_child(win)
	yield(get_node("win/AnimationPlayer"), "finished")
	self.queue_free()
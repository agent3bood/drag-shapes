extends Area2D

var drag = true # track the deag event
var touchEndTimer = 0.0 # timeout for the drag flag

func _ready():
	# add to group to be checked later by drop area
	add_to_group("drag")
	set_process(true)
	set_process_input(true)

func _process(delta):
	touchEndTimer += delta
	# if no touch more that 0.05 seconds, remove the drag flag
	if(touchEndTimer > 0.05):
		drag = false

func _input(event):
	# _input is called for every event, even if not inside the node
	# move the drag area
	if(drag == true):
		if(
		event.type == InputEvent.SCREEN_TOUCH or 
		event.type == InputEvent.SCREEN_DRAG or
		event.type == InputEvent.MOUSE_MOTION
		):
				self.set_pos(event.pos)
				get_tree().set_input_as_handled()

func _input_event(viewport, event, shape_idx):
	# _input_event is called for events only inside the node
	# set the drag flag
	if(
	event.type == InputEvent.SCREEN_TOUCH or 
	event.type == InputEvent.SCREEN_DRAG or
	event.type == InputEvent.MOUSE_MOTION
	):
		touchEndTimer = 0.0
		drag = true

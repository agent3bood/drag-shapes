extends Area2D

func _ready():
	add_to_group("drop")
	set_process(true)

func _process(delta):
	for overlap in get_overlapping_areas():
		if(overlap.is_in_group("drag") and overlap.get_parent() == self.get_parent()):
			# set the drag pos to the drop pos
			overlap.set_pos(self.get_pos())
			# send dropped signal to parent (drag_drop)
			self.get_parent().emit_signal("dropped")
			# play drop sound
			self.get_node("SamplePlayer").play("drop")
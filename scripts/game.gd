extends Node2D
var l1 = preload("res://levels/1.tscn")
var l2 = preload("res://levels/2.tscn")
var l3 = preload("res://levels/3.tscn")
var l4 = preload("res://levels/4.tscn")
var l5 = preload("res://levels/5.tscn")
var l6 = preload("res://levels/6.tscn")
var l7 = preload("res://levels/7.tscn")
var l8 = preload("res://levels/8.tscn")
var l9 = preload("res://levels/9.tscn")
var l10 = preload("res://levels/10.tscn")
var l11 = preload("res://levels/11.tscn")
var l12 = preload("res://levels/12.tscn")
var l13 = preload("res://levels/13.tscn")
var l14 = preload("res://levels/14.tscn")
var l15 = preload("res://levels/15.tscn")
var l16 = preload("res://levels/16.tscn")
var l17 = preload("res://levels/17.tscn")
var l18 = preload("res://levels/18.tscn")
var l19 = preload("res://levels/19.tscn")
var l20 = preload("res://levels/20.tscn")
var l21 = preload("res://levels/21.tscn")
var l22 = preload("res://levels/22.tscn")
var l23 = preload("res://levels/23.tscn")
var l24 = preload("res://levels/24.tscn")
var levels = []
var currentLevel = 0

func _ready():
	levels.push_back(l1)
	levels.push_back(l2)
	levels.push_back(l3)
	levels.push_back(l4)
	levels.push_back(l5)
	levels.push_back(l6)
	levels.push_back(l7)
	levels.push_back(l8)
	levels.push_back(l9)
	levels.push_back(l10)
	levels.push_back(l11)
	levels.push_back(l12)
	levels.push_back(l13)
	levels.push_back(l14)
	levels.push_back(l15)
	levels.push_back(l16)
	levels.push_back(l17)
	levels.push_back(l18)
	levels.push_back(l19)
	levels.push_back(l20)
	levels.push_back(l21)
	levels.push_back(l22)
	levels.push_back(l23)
	levels.push_back(l24)
	load_game()

func _process(delta):
	if(currentLevel >= levels.size()):
		# reset the game
		currentLevel = 0
		
	if(self.get_child_count() == 0):
		save_game()
		var level = levels[currentLevel].instance()
		currentLevel += 1
		self.add_child(level)
		
func save_game():
	var save_currentlevel = File.new()
	save_currentlevel.open("user://save_currentlevel.save", File.WRITE)
	save_currentlevel.store_line(String(currentLevel))
	save_currentlevel.close()
	
func load_game():
	var save_currentlevel = File.new()
	if save_currentlevel.file_exists("user://save_currentlevel.save"):
		save_currentlevel.open("user://save_currentlevel.save", File.READ)
		currentLevel = int(save_currentlevel.get_line())
	set_process(true)
	save_currentlevel.close()
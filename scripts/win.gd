extends Node2D

# the win scene will auto plat the animation
# this script will auto play random sound from the library
func _ready():
	var sample_list = get_node("SamplePlayer").get_sample_library().get_sample_list()
	var rand = randi() % sample_list.size()
	var rand_sound = sample_list[rand]
	self.get_node("SamplePlayer").play(rand_sound)
	
	
	
extends Node2D

signal dropped

func _ready():
	self.add_to_group("dragging")
	self.add_to_group("drag_drop")
	self.connect("dropped", self, "on_dropped")

func on_dropped():
	# remove dragging group
	# in parent if dragging == 0, then free the scene
	self.remove_from_group("dragging")
	for child in self.get_children():
		child.set_process(false)
		child.set_process_input(false)
